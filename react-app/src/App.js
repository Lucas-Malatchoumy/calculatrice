import './App.css';
import React from "react";
import Button from "./Button";

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {data: ''}
    this.result = {data: ''}
  }
  handleClick(e) {
    const value = e.target.getAttribute('value')
    if (value === 'AC') {
      this.setState((state, props) => ({data: ''}))
    } else if (value === '=') {
      console.log(this.state.data);
      this.Calcul()
    }else if (value === 'inverse') {
      this.setState((state, props) => ({data: -(this.state.data) + value}))
    } else {
        this.setState((state, props) => ({data: this.state.data + value}))
        console.log(this.state.data);
      }
  }

  Clear() {
    if (this.state.data === this.result){
      this.setState((state, props) => ({data: ''}))
      console.log(this.state.data);
    }
  }
  Calcul() {
    this.result = eval(this.state.data)
    console.log(this.result);
    this.setState({data: this.result})
    console.log(this.state.data);
    this.Clear()
  }
  render() {
    return (
      <div className="App">
        <div className='flexbox'>
          <div className='flexbox2'>
            <Button onClick={this.handleClick.bind(this)} value='1' />
            <Button onClick={this.handleClick.bind(this)} value='2' />
            <Button onClick={this.handleClick.bind(this)} value='3' />
            <Button onClick={this.handleClick.bind(this)} value='4' />
            <Button onClick={this.handleClick.bind(this)} value='5' />
            <Button onClick={this.handleClick.bind(this)} value='6' />
            <Button onClick={this.handleClick.bind(this)} value='7' />
            <Button onClick={this.handleClick.bind(this)} value='8' />
            <Button onClick={this.handleClick.bind(this)} value='9' />
            <Button onClick={this.handleClick.bind(this)} value='0' />
            <Button onClick={this.handleClick.bind(this)} value='.' />
          </div>
          <div className='flexbox2'>
            <Button onClick={this.handleClick.bind(this)} value='+' />
            <Button onClick={this.handleClick.bind(this)} value='-' />
            <Button onClick={this.handleClick.bind(this)} value='/' />
            <Button onClick={this.handleClick.bind(this)} value='*' />
            <Button onClick={this.handleClick.bind(this)} value='=' />
          </div>
          <div className='flexbox2'>
            <Button onClick={this.handleClick.bind(this)} value='AC'  />
            <Button onClick={this.handleClick.bind(this)} value='inverse' />
            <Button onClick={this.handleClick.bind(this)} value='%' />
          </div>
          <div className='flexbox2'>
            <p>
              Valeur =
              {this.state.data}
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
